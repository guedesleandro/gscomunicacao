<?php
/**
 * @package WordPress
 */

add_theme_support('menus');

	register_nav_menus( array(
		'menu' => __( 'Menu', 'gscomunicacao' )
	) );


function has_children($child_of = null)
{
        if(is_null($child_of)) {
                global $post;
                $child_of = ($post->post_parent != '0') ? $post->post_parent : $post->ID;
        }
        return (wp_list_pages("child_of=$child_of&echo=0")) ? true : false;
}


function the_slug() {
$post_data = get_post($post->ID, ARRAY_A);
$slug = $post_data['post_name'];
return $slug; }



///excerpt
function excerpt($limit) {
        
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt)>=$limit) {
                array_pop($excerpt);
                $excerpt = implode(" ",$excerpt).'...';
        } else {
                $excerpt = implode(" ",$excerpt);
        }

        $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
        return $excerpt;
}




function is_subpage() {
    global $post;

    if ( is_page() && $post->post_parent ) {
        return $post->post_parent;

    } else {
        return get_the_ID();
    }
}