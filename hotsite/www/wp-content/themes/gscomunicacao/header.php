<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>
<?php 
		if (is_home()){
            bloginfo('name');
        }elseif (is_category()){
            single_cat_title(); echo ' -  ' ; bloginfo('name');
        }elseif (is_single()){
            single_post_title();
        }elseif (is_page()){
            bloginfo('name'); echo ': '; single_post_title();
        }else {
            wp_title('',true);
        } ?>
</title>
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />

<?php wp_head(); ?>
</head>

<body>
<div id="site">
	<div id="top">

		<div id="logo"><a href="<?php echo get_page_link(42); ?>" title="GS Comunicação">GS Comunicação</a></div><!--logo-->

		<div id="five">
			<img src="<?php bloginfo('template_directory'); ?>/img/5anos.png" width="90" height="85" alt="GS Comunicação">
		</div><!--five-->
	</div><!--top-->