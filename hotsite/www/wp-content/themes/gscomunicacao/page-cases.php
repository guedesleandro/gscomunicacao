<?php
/**
 * Template Name: Cases
 * Description: Cases.
 *
 * @package WordPress
 */
 get_header();?>

	<div id="content">
		<div id="threehundredandsixty"></div><!--threehundredandsixty-->
<div class="title-page"><?php the_title();?></div><!--title-page-->
<div class="clear"></div>

<?php
$post_obj = $wp_query->get_queried_object();
$post_ID = $post_obj->ID;
$post_title = $post_obj->post_title;
$post_name = $post_obj->post_name;

?>
		<div class="conteudo box <?php echo $post_name; ?>" >
			<?php query_posts('category_name=cases');?>
			<?php if (have_posts()): while (have_posts()) : the_post();?>

			<div class="cases-list">
				<a class="cases-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<?php the_excerpt(); ?>
			</div><!--cases-->

			<?php endwhile; else:?>
			<?php endif;?> 
<a href="javascript:javascript:history.go(-1)" id="fechar">FECHAR</a>
		</div><!--conteudo-->


<a href="<?php echo get_page_link(42); ?>" id="voltar">< MENU</a>

	</div><!--content-->

<?php get_footer();?>