<?php
/*
Plugin Name: Café Azul
Plugin URI: http://cafeazul.com.br
Description: Plugin da Agência Digital Café Azul
Version: 0.1b
Author: Café Azul
Author URI: http://cafeazul.com.br
*/


/**
 * Custom Login Logo
 */
function custom_login_logo() {
?>
  	<style type="text/css">
  		.login h1{
  			text-align: center;
  		} 
		.login h1 a {
		    background-image: url("http://www.cafeazul.com.br/modelo_teste/logo.png");
		    background-position: center top;
		    background-repeat: no-repeat;
		    background-size: 274px 83px;
		    display: block;
		    height: 83px;
		    outline: 0 none;
		    margin: 0 auto;
		    overflow: hidden;
		    padding-bottom: 15px;
		    text-indent: -9999px;
		    width: 274px;
		}
	</style>
<?php
}
add_action( 'login_form', 'custom_login_logo' );

/**
 * Custom SUBMIT Button for BOOTSTRAP
 */
function submit_to_button_tag( $button, $form ) {
  return '<button type="submit" class="btn btn-primary">'. $form["button"]["text"] .'</button>';
}
add_filter( 'gform_submit_button', 'submit_to_button_tag', 10, 2 );

?>