<?php get_header();?>

	<div id="page">
		<?php if (have_posts()): while (have_posts()) : the_post();?>
		<div id="left">
			<img src="<?php bloginfo('template_directory'); ?>/img/menu-imgs-<?= the_slug(); ?>.png">
		</div><!--left-->

		<div id="right">
			<span><a href="<?php echo home_url(); ?>" id="link-home">HOME</a></span>
			<div id="box">
				
				
				<?php the_content(); ?>
			</div><!--box-->

		</div><!--right-->
		<?php endwhile; else:?>
		<?php endif;?> 
	</div><!--page-->

<?php get_footer();?>