<?php
/**
 * @package WordPress
 */

//imagem destacada
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}
add_image_size( 'destaque', 342, 108, true );
add_image_size( 'banner', 440, 391, true );
add_image_size( 'img-big-galery', 370, 770, true );

function the_slug($echo = true) {
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  do_action('after_slug', $slug);
  return $slug;
}

function excerpt($limit) {
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt)>=$limit) {
                array_pop($excerpt);
                $excerpt = implode(" ",$excerpt).'...';
        } else {
                $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
        return $excerpt;
}
/*
add_filter('post_gallery', 'galeria', 10, 2);

function galeria($output, $attr) {
  var_dump($attr);
  return $output;
}
*/
function getIdGalleryOrder($postId) {
  $currentPage = get_post($postId);
  $content = $currentPage->post_content;
  preg_match_all('/(ids="(\d*,?"?)+)/',$content, $result, PREG_PATTERN_ORDER);
  preg_match_all('/(\d+)/',$result[0][0], $output, PREG_PATTERN_ORDER);
  $ids = implode(",",$output[0]);
  return $ids;
}



//contar quantos posts tem em cada categoria
function wp_get_cat_postcount($id) {
    $cat = get_category($id);
    $count = (int) $cat->count;
    $taxonomy = 'category';
    $args = array(
      'child_of' => $id,
    );
    $tax_terms = get_terms($taxonomy,$args);
    foreach ($tax_terms as $tax_term) {
        $count +=$tax_term->count;
    }
    return $count;
}