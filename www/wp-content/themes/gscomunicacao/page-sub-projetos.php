<?php 
/**
 * Template Name: Sub-Portfólio
 * Description: Modelo para sub-paginas da Portfólio.
 *
 * @package WordPress
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
	<?php
    global $page, $paged;
    wp_title( '-', true, 'right' );
    bloginfo( 'name' );
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        echo " - $site_description";
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf( 'Página %s' ), max( $paged, $page );
    ?>
</title>
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.all.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.tinycarousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/default.js"></script>

</head>

<body>
<style type="text/css">
 body{
 	background: #F9F9F9;
 }
</style>

<?php if (have_posts()): while (have_posts()) : the_post();?>

	<div class="iframe">
		<div id="galeria">

<div id="slider1">
        <a class="buttons prev" href="#">left</a>
        <div class="viewport">
          <ul class="overview">
			    <?
			    
			      $include = getIdGalleryOrder($post->ID);
			    
  				  $args = array(
  					  'post_parent'    => $post->ID,
  					  'post_type'      => 'attachment',
  					  'post_mime_type' => 'image',
  					  'orderby'        => 'post__in',
  					  //'order'          => 'DESC',
  					  'numberposts'    => -1,
  					  'include'        => $include
  				  );
  				  $images = get_children($args);
  				  $firstFullImage = null;
				  ?>
				  <? foreach ($images as $attachment_id => $attachment ): ?>
				      <? if($attachment->post_excerpt): ?>
				      <? $imageFull = wp_get_attachment_image_src( $attachment->ID, 'large' ); ?>
              <li><a href="#" title="<?= $attachment->post_excerpt; ?>" rel="<?= $imageFull[0]; ?>" img-w="<?= $imageFull[1]; ?>" img-h="<?= $imageFull[2]; ?>"><?= wp_get_attachment_image( $attachment->ID, 'thumbnail' ); ?></a></li>
				      
				      <? if($firstFullImage == null) {
				          $firstFullImage = $imageFull; 
				          $firstFullImage[3] = $attachment->post_excerpt;
				          }
				      ?>		      
				      <? endif; ?>
				  <? endforeach; ?>
				  <?
				
					$videos = get_field('projetos_video');
					foreach($videos as $video): ?>
			<li><a href="#" title="Vídeo" rel="<?= $video; ?>" isVideo="true"><img src="http://i1.ytimg.com/vi/<?= $video; ?>/mqdefault.jpg" width="90" height="90"></a></li>
	          	
					<? endforeach; ?>
			</ul>

</div>
    <a class="buttons next" href="#">right</a>
  </div>
      <img class="img-big" src="<?= $firstFullImage[0]; ?>">
	  <iframe id="youtube_frame" style="display: none;" width="701" height="417" src="#" frameborder="0" allowfullscreen></iframe>
		</div><!--galeria-->
    	<span id="legenda"><?= $firstFullImage[3]; ?></span>
	</div><!--iframe-->

<?php endwhile; else:?>
<?php endif;?> 


</body>
</html>