<?php get_header();?>

	<div id="page">


		<div id="left">
			<img src="<?php bloginfo('template_directory'); ?>/img/menu-imgs-projetos.png">
		</div><!--left-->

		<div id="right">
			<span><a href="<?php echo home_url(); ?>" id="link-home">HOME</a></span>
			<div class="portfolio">


				<img src="<?php bloginfo('template_directory'); ?>/img/projetos.gif" border="0" usemap="#Map" />
				<map name="Map" id="Map">
				  <area shape="rect" coords="-1,0,97,163" href="<?php echo get_page_link( 27 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="97,-2,220,137" href="<?php echo get_page_link( 29 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="96,253,220,390" href="<?php echo get_page_link( 47 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="219,-1,313,165" href="<?php echo get_page_link( 74 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="220,163,313,297" href="<?php echo get_page_link( 53 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="312,0,433,139" href="<?php echo get_page_link( 37 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="0,162,97,297" href="<?php echo get_page_link( 41 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="96,135,220,255" href="<?php echo get_page_link( 44 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="312,138,432,255" href="<?php echo get_page_link( 49 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="1,296,96,390" href="<?php echo get_page_link( 51 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="219,296,313,391" href="<?php echo get_page_link( 55 ); ?>" class="lbp_secondary" rel="lightbox" />
				  <area shape="rect" coords="313,254,436,397" href="<?php echo get_page_link( 57 ); ?>" class="lbp_secondary" rel="lightbox" />
				</map>

			</div><!--box-->
		</div><!--right-->
		

	</div><!--page-->

<?php get_footer();?>