<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title><?php
        global $page, $paged;
        wp_title( '-', true, 'right' );
        bloginfo( 'name' );
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " - $site_description";
        if ( $paged >= 2 || $page >= 2 )
            echo ' | ' . sprintf( 'Página %s' ), max( $paged, $page );
        ?></title>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-icon" />
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.cycle.all.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.tinycarousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/default.js"></script>

<?php wp_head(); ?>
</head>

<body>
<div class="site">

	<div id="header">
		<div id="logo">
			<h1><a href="<?php echo home_url(); ?>">GS Comunicação</a></h1>
		</div><!--logo-->

		<div id="header-right">
			<h3>MOVIDA A DESAFIOS</h3>
		</div><!--header-right-->
	</div><!--header-->