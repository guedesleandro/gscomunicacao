<?php get_header();?>

	<div id="home">
		<div id="menu">
			<ul>
				<a href="<?php echo get_page_link( 7 ); ?>"><li id="item-menu-1"></li></a>
				<a href="<?php echo get_page_link( 11 ); ?>"><li id="item-menu-2"></li></a>
				<a href="/eventos"><li id="item-menu-3"></li></a>
				<a href="<?php echo get_page_link( 15 ); ?>"><li id="item-menu-4"></li></a>
				<a href="<?php echo get_page_link( 17 ); ?>"><li id="item-menu-5"></li></a>
			</ul>
		</div><!--menu-->

		<div id="banner">
			<?php query_posts('posts_per_page=4&post_type=page&post_parent=282&orderby=menu_order&order=ASC');?>
				<?php if (have_posts()): while (have_posts()) : the_post();?>

				<div class="banner-loop">
					<a href="<?php the_permalink(); ?>">

					<?php if ( function_exists( 'the_post_thumbnail' ) )
						the_post_thumbnail('banner'); ?>
					</a>
					<p><?php echo excerpt(20); ?></p>
				</div><!--banner-loop-->
				
				<?php endwhile; else:?>
				<?php endif;?>

		</div><!--banner-->
	</div><!--home-->

<?php get_footer();?>