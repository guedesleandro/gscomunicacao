<?php 
/**
 * Template Name: Sub-destaque
 * Description: Modelo para sub-paginas da Portfólio.
 *
 * @package WordPress
 */
?>
<?php get_header();?>

  <div id="page">
    <?php if (have_posts()) while (have_posts()) : the_post();?>
    <div id="left">
      <img src="<?php bloginfo('template_directory'); ?>/img/imgs-<?= the_slug(); ?>.png">
    </div><!--left-->

    <div id="right">
      <span><a href="<?php echo home_url(); ?>" id="link-home">HOME</a></span>

        <a href="<?= get_permalink(get('destaque_link_direto_do_portfolio')); ?>" class="lbp_secondary" rel="lightbox" ><img src="<?= get('destaque_imagem_apos_clicar_no_banner'); ?>" alt="<?php the_title(); ?>">  </a>
        
    </div><!--right-->
    <?php endwhile; ?>
  </div><!--page-->

<?php get_footer();?>

