$(document).ready(function(){

	//carousel
	$('#slider1').tinycarousel();
	$('#slider2').tinycarousel({ pager: true, interval: true  });

	$('#banner').before('<div id="nav">').cycle({ 
	    fx:     'fade', 
	    speed:  'fast', 
	    timeout: 3000, 
	    pager:  '#nav' 
	});

	  $("ul.overview li a").click(function() {
		if($(this).attr('isVideo') == "true") {
			youtubeURL = "http://www.youtube.com/embed/" + $(this).attr('rel');
			$('#youtube_frame').attr('src', youtubeURL);
			$('.img-big').hide();
			$('#youtube_frame').show();
		}
		else {
			$('#youtube_frame').hide();
			$('.img-big').show();
    		rel = $(this).attr('rel');
		    $('.img-big').attr('src', rel);
		    $('.img-big').attr('width', $(this).attr('img-w'));
		    $('.img-big').attr('height', $(this).attr('img-h'));
		}
	    $('#legenda').html($(this).attr('title'));
	    return false;
  });
});