<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'gscomunicacao');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', '127.0.0.1');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y&<V;0TBsABSt&>AU1B+d?|t5U aAI2Nn(]S3~Y$,3a>K1E|iU$A5u:-Mp62<9A+');
define('SECURE_AUTH_KEY',  'TJ@P]MNa]N<C%/J*s7+Uz`U!fqi`z8[t&#7LI^RKhm-X7FH=_^;ft7&A|YYpwp0b');
define('LOGGED_IN_KEY',    '/RCV-|-{#(CUI|`t6|EI-,Z|)1|&^F[y`Z3(Khn){pZStG`T[JjCl7sQ47k5kUAr');
define('NONCE_KEY',        '|smrbJ(|jomNh||!n2b{27QQQ/DK8Bp{qys|HKI.q-z#M~Uq~!MYJ=LJ]P5K.J~.');
define('AUTH_SALT',        '9=sFt5^ cJjli -e??fH=*+t-u-`Tj^N:82A1!v(3j:`$C-zQcGo?JZN^qMzWH}X');
define('SECURE_AUTH_SALT', 'nk}{=wIt7RC+7TYCbcoZF|dCji#]46!W,@Pk8f3ez+OBPkHN]KB}pu=<b{*%wj*$');
define('LOGGED_IN_SALT',   '._+)yA9-c^`I]a%9Z}pQ4PWQlY&@(gg>Ev-HnAQruVI=4P=ii^e:POz;*H+:y#oj');
define('NONCE_SALT',       '[hWvsFd>VA+G&WEVOA ^ HtX9Mn;}+703jy3P=Te|v_k]5Zfu^bq~njFRep*[0F|');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
