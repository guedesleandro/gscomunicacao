<?php 
/**
 * Template Name: Solucoes
 * Description: Modelo para solucoes.
 *
 * @package WordPress
 */
?>
<?php get_header();?>
<?php if (have_posts()): while (have_posts()) : the_post();?>
<div id="content" class="page">
  <div id="right">
    <div id="page-top"><h2><?php the_title( ); ?></h2></div><!--page-top-->
    <div id="box">
      <div id="box-conteudo">
      <?php the_content(); ?>
    </div><!--box-conteudo-->
      <a href="<?php echo home_url(); ?>" id="close-back">[ x ] FECHAR</a>
    </div><!--box-->
  </div><!--right-->
</div><!--content-->
<?php endwhile; else:?>
<?php endif;?> 
<?php get_footer();?>

