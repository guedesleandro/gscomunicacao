
<?php get_header();?>
<?php if (have_posts()): while (have_posts()) : the_post();?>
<div id="content" class="page">
  <div id="right">
    <div id="page-top"><h2><?php the_title(); ?></h2></div><!--page-top-->
    <div id="box">
      <div id="box-conteudo">
      <?php the_content(); ?>
    </div><!--box-conteudo-->
    <?php $parent_id = get_post($post_id)->post_parent; ?>
      <a href="<?php echo get_page_link( $parent_id ); ?>" id="close-back">< VOLTAR</a>
    </div><!--box-->
  </div><!--right-->
</div><!--content-->
<?php endwhile; else:?>
<?php endif;?>
<?php get_footer();?>

