<?php 
/**
 * Template Name: serviços
 * Description: Modelo para serviços.
 *
 * @package WordPress
 */
?>
<?php get_header();?>
	<div id="content">
	  <div id="right" class="eventos-servicos">
	   
	  	<img id="img-servicos" src="<?php bloginfo('template_directory'); ?>/img/stand.jpg">
	  	<ul id="ul-servicos">
			<?php query_posts('post_parent=6&post_type=page&order=ASC');?>
			<?php if (have_posts()): while (have_posts()) : the_post();?>

			<div class="fix-servicos-ul"><li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li></div>
						
			<?php endwhile; else:?>
			<?php endif;?>
			<?php wp_reset_query(); ?>	
		</ul>
		<a href="<?php echo home_url(); ?>" id="go-home">< HOME</a>
	  </div><!--right-->
	</div><!--content-->

<?php get_footer();?>

