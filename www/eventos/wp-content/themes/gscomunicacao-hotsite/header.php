<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title><?php
        global $page, $paged;
        wp_title( '-', true, 'right' );
        bloginfo( 'name' );
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " - $site_description";
        if ( $paged >= 2 || $page >= 2 )
            echo ' | ' . sprintf( 'Página %s' ), max( $paged, $page );
        ?></title>
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,100,300,500' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>

<body>
<div id="site">
	<div id="top">

		<div id="logo"><a href="<?php echo home_url(); ?>">GS Comunicação</a></div><!--logo-->

		<div id="five">
			<img src="<?php bloginfo('template_directory'); ?>/img/5anos.png" width="90" height="85" alt="GS Comunicação">
		</div><!--five-->
	</div><!--top-->