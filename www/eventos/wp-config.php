<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'gs');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q.#fZl0%`o7@5?@5}t+7?2^Z=.OaoJorlR7oTZSJN]aeiDKa+8Vmy)S|&VN$94x$');
define('SECURE_AUTH_KEY',  'A$>+wjOO@)>8vl}pf3c*+i=FZvnZ=kz+V<`L%7Wi!925~%UJ8}!g7/RF[ujmGV]>');
define('LOGGED_IN_KEY',    '-!nuwjK:ga%302&k-s1N<#=lwW+@4:@Z-|U+y-i-?T%rL SU.$3ppY3_]w@;:+VK');
define('NONCE_KEY',        ') nW xOo39-RK-D?[RG6HpW/M{a><-URMaJ#A8mtWH7KI|-`mC)=04Nbz)<C%rI:');
define('AUTH_SALT',        '4a@Kz<d!+0I mbw!u^2zTen(e?(WB*|CWkZB8E+Ie}(X+rWc-HKrk@tZ)h<l2;9p');
define('SECURE_AUTH_SALT', '@<O UR~7PRA=dl:$ jC,Gd/^)V$@Tyzk:!3VW;?z^TDr(jZ(w.g|)^RwvWN*vIhe');
define('LOGGED_IN_SALT',   '{4*}l<k5vl:I2${!jmzxbF!TH`weitx;`cOAmPr:=Mmh@IP6M/%b6O{XS,GW0*6J');
define('NONCE_SALT',       '#i~ J>W2+XD_S:I}@!n }mrx2GlUJOuUt+Bs<R1@bg-,dW0cW4F4*}[wYJ*d(42`');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'hotsite_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
